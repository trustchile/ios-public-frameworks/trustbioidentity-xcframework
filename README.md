## Trust Technologies
![image](https://avatars2.githubusercontent.com/u/42399326?s=200&v=4)

## Trust BioIdentity
TrustBioidentity is an SDK responsible for providing identity validation through a photograph of a person's fingerprint using the camera of a mobile device

## Table of contents
- [Implementation](#implementation)
- [Initialize](#initialize)
    * [Builder](#builder)
    * [Example](#example-builder)
- [Usage](#usage)
    * [TrustBioIdentity Methods](#trustbioidentity-methods)
- [Errors](#errors)
    * [Codes](#codes)
___
# Implementation
Easiest way to install TrustBioidentity is by using Cocoapods within just a few steps:

1. Initialize cocoapods in your project (if it's not already done):
```bash
pod init
```
*this would generate a **Podfile** and **Pods** folder.*

2. Complete Podfile according to your needs:
```ruby
platform :ios, '13.0'

# this indicates the source where Cocoapods would find the pod to install

target 'AppName': do
use_frameworks!
...

# pod name and version to install
pod 'TrustBioIdentity', :git => 'https://gitlab.com/trustchile/ios-public-frameworks/trustbioidentity-xcframework', :tag => '2.0.0'

...
end
```

3. Finally install previously specified dependencies using:
```bash
pod install
``` 
---
# Initialize
#### Builder
| Name | Description | Default Value | Optional |
|---|---|---|---|
| nin | DNI of the user | - |No|
| country | Specifies the country | CHL | No|
| index | - | - | - |
| strategyType | Specifies the finger | FINGER| No|
| companyId | Specifies the id of the company | - | No |
| provider | Specifies the provider company | TRUST| No|
| clientId | Specifies the client Id | - | No|
| clientSecret | Specifies the client secret | - | No|
| transactionId | Specifies the transaction Id | null | Yes|

#### Example Builder
```swift
var bioIdentity = TrustBioIdentity.builder
    .nin("123456789")
    .companyId(1)
    .index(2)
    .clientId(<YOUR_CLIENT_ID>)
    .clientSecret(<YOUR_CLIENT_SECRET>)
    .provider(Provider.trust)
    .country(Country.chile)
    .strategyType(Strategy.finger)
    .transactionId("12345")
    .build()
```
___
# Usage
# TrustBioIdentity methods

#### Request validation
This methods allows client application to ask its user for a fingerprint validation by making a picture of its finger.
```swift
bioIdentity.requestValidation()
```

#### Request enroll
This methods allows client application to ask its user for a fingerprint enrollment by making a picture of its finger.
```swift
bioIdentity.requestEnroll()
```

# Errors
TrustError definition:
- **code**: Code of error [check specification](#codes)
- **localizedDescription**: Error description as String

## Codes
Possible errors that can be found when integrating the SDK
|  Code | Value |
|---|---|
|    0 | Unknown error |
|  400 | Bad request |
|  401 | The request was unauthorized |
|  403 | The request was forbidden |
|  404 | The requested could not be found |
|  405 | Method not allowed |
|  500 | Internal server error |
|  501 | Not implemented |
|  502 | Bad gateway |
|  503 | Service unavailable |
| 2000 | Error code bioidentity response |
| 2003 | Unsupported code bioidentity response |
| 9000 | Server error |
