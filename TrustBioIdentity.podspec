Pod::Spec.new do |s|
	s.platform              = :ios
	s.ios.deployment_target = "13.0"
	s.swift_version         = "5"
	s.requires_arc          = true
	s.name                  = "TrustBioIdentity"
	s.version               = "1.1.1"
	s.homepage              = "https://gitlab.com/trustchile/ios-public-frameworks/trustbioidentity-xcframework"
	s.author                = { "Diego Villouta" => "dvillouta@jumpitt.com" }
	s.summary               = "Library to enroll and validate fingerprints with a dni through a view controller"
	s.license               = { :type => "MIT", :file => "LICENSE" }
	s.source                = { :git => "https://gitlab.com/trustchile/ios-public-frameworks/trustbioidentity-xcframework.git", :tag => "#{s.version}" }
	s.frameworks            = "UIKit"
	s.vendored_frameworks   = "TrustBioIdentity.xcframework"
	s.dependency 'MetalPetal'
	s.dependency 'TensorFlowLiteSwift'
end
